# Dashboard
Dashboard component wraps existing visualization and allows interaction between them.
## Usage
```angular2html
<kypo-dashboard [trainingInstanceId]="trainingInstanceId"></kypo-dashboard>
```
