### 16.0.0 Update to Angular 16 and update local issuer to keycloak.
* cb47a46 -- [CI/CD] Update packages.json version based on GitLab tag.
* e51c4d3 -- Merge branch '15-update-to-angular-16' into 'master'
* fa2d278 -- Update local issuer to keycloak
* 594e0d7 -- Update to Angular 16
### 15.0.0 Update to Angular 15.
* d61b100 -- [CI/CD] Update packages.json version based on GitLab tag.
*   306e57c -- Merge branch '14-update-to-angular-15' into 'master'
|\  
| * 33aa50a -- Update with new Angular 15 libraries
| * d0be953 -- Merge branch '396296-master-patch-15877' into 'master'
* | 36b1ded -- Merge branch '396296-master-patch-15877' into 'master'
|/  
* 3db366a -- Update README.md
### 14.3.0 Bump clustering visualization to address changes in max achievable score.
* de275e80 -- [CI/CD] Update packages.json version based on GitLab tag.
* dcf2d2a3 -- Merge branch 'bump-overview-visualization-to-latest' into 'master'
* ecf161aa -- Bump overview
### 14.2.0 Bump hurdling visualization and address changes in inputs.
* c2bb1da3 -- [CI/CD] Update packages.json version based on GitLab tag.
* 05aef64d -- Merge branch '13-address-changes-in-hurdling-visualization' into 'master'
* 581a38be -- Resolve "Address changes in hurdling visualization"
### 14.1.0 Remove elastic search service url.
* 41f13c3f -- [CI/CD] Update packages.json version based on GitLab tag.
* bd85829d -- Merge branch '12-remove-elastic-search-service-from-configs' into 'master'
* 6d9f9045 -- Resolve "Remove elastic search service from configs"
### 14.0.1 Rename kypo2 to kypo.
* 7d4bfe2f -- [CI/CD] Update packages.json version based on GitLab tag.
* eb5f8b21 -- Rename kypo2 to kypo
### 14.0.0 Update to Angular 14.
* 35c4f847 -- [CI/CD] Update packages.json version based on GitLab tag.
* e8a4c9b6 -- Merge branch '10-update-to-angular-14' into 'master'
* 88d4a594 -- Resolve "Update to Angular 14"
### 13.0.1 Fix problems with timeline in clustering and line visualizations.
* 2a61ce54 -- [CI/CD] Update packages.json version based on GitLab tag.
* aa5429a0 -- Merge branch '9-bump-dashboard-visualizations' into 'master'
* cd01d18b -- Resolve "Bump dashboard visualizations"
### 13.0.0 Update to Angular 13, CI/CD update.
* c1621b84 -- [CI/CD] Update packages.json version based on GitLab tag.
* f1cb6101 -- Merge branch '8-update-to-angular-13' into 'master'
* 7111e447 -- Resolve "Update to Angular 13"
### 12.1.0 Fix problems with trainee selection, zoom reset and Walkthrough title.
* 5c4f8066 -- [CI/CD] Update packages.json version based on GitLab tag.
* 6bc062fe -- Merge branch '7-dashboard-improvements' into 'master'
* 62a0f77d -- Resolve "dashboard improvements"
### 12.0.3 Responsivity problems, interactions between visualizations adjusted.
* b173b08b -- [CI/CD] Update packages.json version based on GitLab tag.
* 7ef6c13b -- Merge branch '6-adust-dashboard-according-to-release-testing' into 'master'
* 0e463ad2 -- Resolve "Adust dashboard according to release testing"
### 12.0.2 Fix style issues.
* 320c507e -- [CI/CD] Update packages.json version based on GitLab tag.
* 4bafd2f2 -- Merge branch '5-pre-release-changes' into 'master'
* 4e753ecc -- Fix style problems
### 12.0.1 Refactored visualization dashboard. Added Summary and Reference graph component for definitions with reference solutions.
